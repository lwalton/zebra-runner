﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerDownScript : MonoBehaviour {
    private HUDScript hud;

    void Start () { }

    void OnTriggerEnter2D (Collider2D other) {
        if (other.tag == "Player") {
            hud = GameObject.Find ("Main Camera").GetComponent<HUDScript> ();
            hud.upFalse ();
            hud.IncreaseScore (-10);
            hud.downTrue ();
            //TODO remove this for web version
           if (PlayerPrefs.GetString ("vibration").Equals ("on")) {
               Handheld.Vibrate ();
           }
            Destroy (this.gameObject);
        }
    }
}