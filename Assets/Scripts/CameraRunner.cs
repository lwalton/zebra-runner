﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRunner : MonoBehaviour {
    public Transform player;

    void Start () {

    }

    void Update () {
        if (player) {
            transform.position = new Vector3 (player.position.x + 6, 0, -10);
        }
    }
}