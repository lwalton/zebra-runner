﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
// using Firebase;

public class StartScreenScript : MonoBehaviour {
    public Font theFont;
    private float startX, lengthX, lengthY;
    private GUIStyle buttStyle, style;

    void Start () {
        PlayerPrefs.SetString ("JustPlayed", "false");
        if (!(PlayerPrefs.HasKey ("vibration"))) {
            PlayerPrefs.SetString ("vibration", "on");
        }

        Screen.SetResolution (1280, 720, false, 0);

        startX = (3f / 8f) * Screen.width;
        lengthX = (1f / 4f) * Screen.width;
        lengthY = (1f / 10f) * Screen.height;

        buttStyle = new GUIStyle ("button");
        buttStyle.font = theFont;
        buttStyle.fontSize = 25;
    }

    void Update () {
        buttStyle = new GUIStyle ("button");
        buttStyle.font = theFont;
        buttStyle.fontSize = 25;

        startX = (3f / 8f) * Screen.width;
        lengthX = (1f / 4f) * Screen.width;
        lengthY = (1f / 10f) * Screen.height;
    }

    void OnGUI () {

        if (GUI.Button (new Rect (startX, (8f / 30f) * Screen.height, lengthX, lengthY), "Start game", buttStyle)) {
            //Starts game
            SceneManager.LoadScene (5);
        }
        if (GUI.Button (new Rect (startX, (12f / 30f) * Screen.height, lengthX, lengthY), "Help", buttStyle)) {
            //Loads help page
            SceneManager.LoadScene (3);
        }
        if (GUI.Button (new Rect (startX, (16f / 30f) * Screen.height, lengthX, lengthY), "High scores", buttStyle)) {
            //Loads end screen / high score screen
            SceneManager.LoadScene (2);
        }

        if (GUI.Button (new Rect (startX, (20f / 30f) * Screen.height, lengthX, lengthY), "Quit", buttStyle)) {
            Application.Quit ();

        }
    }
}