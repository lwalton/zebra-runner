﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkySpawn : MonoBehaviour {
    public GameObject obj;
    private HUDScript hud;
    private float speed = 10;

    void Start () {
        Spawn ();
    }

    void Update () {
        float score;

        hud = GameObject.Find ("Main Camera").GetComponent<HUDScript> ();
        score = hud.getPlayerScore ();

        if (score < 0) {
            speed = 10;
        } else {
            speed = (10 - (score / 50));
        }
    }

    void Spawn () {
        Instantiate (obj, transform.position, Quaternion.identity);
        Invoke ("Spawn", speed);
    }

    void OnTriggerEnter2D () {
        Destroy (this.gameObject);
    }
}