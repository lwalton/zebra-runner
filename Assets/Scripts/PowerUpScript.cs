﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpScript : MonoBehaviour {
    private HUDScript hud;

    void Start () { }

    void OnTriggerEnter2D (Collider2D other) {
        if (other.tag == "Player") {
            hud = GameObject.Find ("Main Camera").GetComponent<HUDScript> ();
            hud.downFalse ();
            hud.IncreaseScore (10);
            hud.upTrue ();
            Destroy (this.gameObject);
        }
    }
}