﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FactsScript : MonoBehaviour {
    public Font theFont;
    private float buttLengthX, buttLengthY;
    private GUIStyle buttStyle, styleH;
    private int i;
    private string name2 = "";
    private bool canGo = false;

    void Start () {

        styleH = new GUIStyle ();
        styleH.alignment = TextAnchor.MiddleCenter;
        styleH.fontSize = 50;
        styleH.font = theFont;
        styleH.normal.textColor = Color.black;

        buttLengthX = (6f / 30f) * Screen.width;
        buttLengthY = (3f / 30f) * Screen.height;
        buttStyle = new GUIStyle ("button");
        buttStyle.alignment = TextAnchor.MiddleCenter;
        buttStyle.fontSize = 25;
        buttStyle.font = theFont;
        buttStyle.richText = true;
        name2 = "";
    }

    void Update () {
        nameCheck(name2);
    }

    void OnGUI () {
        GUI.Label (new Rect (0, (1f / 30f) * Screen.height, Screen.width, (2f / 30f) * Screen.height), "Enter name:", styleH);


        name2 = GUI.TextField (new Rect ((1f / 4f) * Screen.width, (10f / 30f) * Screen.height, (1f / 2f) * Screen.width, (2f / 30f) * Screen.height), name2, 25);
        if (canGo) {
            if (GUI.Button (new Rect ((12f / 30f) * Screen.width, (17f / 30f) * Screen.height, buttLengthX, buttLengthY), "Enter", buttStyle)) {

                PlayerPrefs.SetString ("name", nameCheck (name2));
                //Goes to end screen
                SceneManager.LoadScene (2);
            }
        }
    }

    //Only allows letters of digits in the name
    string nameCheck (string name) {
        string st1 = "";
        foreach (char letter in name) {
            if (char.IsLetterOrDigit (letter)) {
                st1 = st1 + letter;
            } else {
            }
        }
        if (st1.Equals ("")) {
            canGo = false;
        } else {
            canGo = true;
        }
        return st1;
    }

}