﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HelpScript : MonoBehaviour {
    public Font theFont;
    private string text = "Tap the screen to jump\nYellow coins are good.\nRed coins are bad.";
    private float labelStartX, labelStartY, buttStartY, buttLengthX, buttLengthY;
    private GUIStyle style, buttStyle, styleH;

    void Start () {
        labelStartX = (1f / 3f) * Screen.width;
        labelStartY = (3f / 30f) * Screen.height;
        buttStartY = (20f / 30f) * Screen.height;
        buttLengthX = (5f / 30f) * Screen.width;
        buttLengthY = (1f / 10f) * Screen.height;

        style = new GUIStyle ();
        style.alignment = TextAnchor.MiddleCenter;
        style.fontSize = 27;
        style.font = theFont;
        style.normal.textColor = Color.black;

        styleH = new GUIStyle ();
        styleH.alignment = TextAnchor.MiddleCenter;
        styleH.fontSize = 50;
        styleH.font = theFont;
        styleH.normal.textColor = Color.black;

        buttStyle = new GUIStyle ("button");
        buttStyle.alignment = TextAnchor.MiddleCenter;
        buttStyle.fontSize = 25;
        buttStyle.font = theFont;
    }

    void Update () { }
    void OnGUI () {
        GUI.Label (new Rect (0, (1f / 30f) * Screen.height, Screen.width, (2f / 30f) * Screen.height), "Help", styleH);

        GUI.Label (new Rect (labelStartX, labelStartY, labelStartX, labelStartX), text, style);

        if (GUI.Button (new Rect ((1f / 30f) * Screen.width, buttStartY, buttLengthX, buttLengthY), "Play", buttStyle)) {
            //Starts game
            SceneManager.LoadScene (5);
        }
        if (GUI.Button (new Rect ((24f / 30f) * Screen.width, buttStartY, buttLengthX, buttLengthY), "Return", buttStyle)) {
            //Returns to menu
            SceneManager.LoadScene (0);
        }
        if (PlayerPrefs.GetString ("vibration").Equals ("on")) {
            if (GUI.Button (new Rect ((10f / 30f) * Screen.width, buttStartY, 2f * buttLengthX, buttLengthY), "Vibration off", buttStyle)) {
                PlayerPrefs.SetString ("vibration", "off");
                SceneManager.LoadScene (3);

            }
        } else {
            if (GUI.Button (new Rect ((10f / 30f) * Screen.width, buttStartY, 2f * buttLengthX, buttLengthY), "Vibration on", buttStyle)) {
                PlayerPrefs.SetString ("vibration", "on");
                SceneManager.LoadScene (3);
            }

        }
    }
}