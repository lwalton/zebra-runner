﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ThreeTwoOneScript : MonoBehaviour {
    public Font theFont;
    private string countdown = "";
    private bool showCountdown = false;
    private float startX, startY, lengthX, lengthY;
    private GUIStyle cr, styleH;

    void Start () {
        styleH = new GUIStyle ();
        styleH.alignment = TextAnchor.MiddleCenter;
        styleH.fontSize = 50;
        styleH.font = theFont;
        styleH.normal.textColor = Color.black;

        showCountdown = true;
        countdown = "3";
        Invoke ("ex1", 1);
        startX = Screen.width / 4f;
        startY = Screen.height / 4f;
        lengthX = Screen.width / 2f;
        lengthY = Screen.height / 2f;
        cr = new GUIStyle ();
        cr.alignment = TextAnchor.MiddleCenter;
        cr.normal.textColor = Color.black;
        cr.fontSize = 150;
        cr.font = theFont;
    }

    void ex1 () {
        countdown = "2";
        Invoke ("ex2", 1);
    }

    void ex2 () {
        countdown = "1";
        Invoke ("ex3", 1);
    }

    void ex3 () {
        countdown = "GO";
        Invoke ("ex4", 1);
    }

    void ex4 () {
        //Loads game after countdown
        SceneManager.LoadScene (1);
    }

    void OnGUI () {
        GUI.Label (new Rect (0, (1f / 30f) * Screen.height, Screen.width, (2f / 30f) * Screen.height), "Ready...", styleH);

        if (showCountdown) {
            GUI.Label (new Rect (startX, startY, lengthX, lengthY), countdown, cr);
        }
    }
}