﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HUDScript : MonoBehaviour {
    public static bool allowed = true;
    public Font theFont;
    public AudioClip buttonSound;
    private float playerScore = 0;
    private GUIStyle style, upStyle, downStyle;
    private bool up, down;
    private float startX, startY, lengthX, lengthY;

    void Start () {
        style = new GUIStyle ();
        style.fontSize = 30;
        style.normal.textColor = Color.white;
        style.font = theFont;

        upStyle = new GUIStyle ();
        upStyle.alignment = TextAnchor.MiddleCenter;
        upStyle.normal.textColor = new Color (254f / 255f, 194f / 255f, 58f / 255f);
        upStyle.fontSize = 200;
        upStyle.font = theFont;

        downStyle = new GUIStyle ();
        downStyle.alignment = TextAnchor.MiddleCenter;
        downStyle.normal.textColor = new Color (147f / 255f, 0, 0);
        downStyle.fontSize = 200;
        downStyle.font = theFont;
        startX = Screen.width / 4;
        startY = Screen.height / 4;

        lengthX = Screen.width / 2;
        lengthY = Screen.height / 2;
    }

    public int getPlayerScore () {
        return (int) playerScore;
    }

    void Update () {
        playerScore += (Time.deltaTime);
    }

    public void IncreaseScore (int amount) {
        playerScore += amount;
    }

    void OnGUI () {
        GUI.Label (new Rect (10, 10, 100, 30), "Score: " + (int) (playerScore), style);

        if (up) {
            GUI.Label (new Rect (startX, startY, lengthX, lengthY), "+10", upStyle);
            Invoke ("upFalse", 0.25f);
        }
        if (down) {
            GUI.Label (new Rect (startX, startY, lengthX, lengthY), "-10", downStyle);
            Invoke ("downFalse", 0.25f);
        }
    }

    public float getScore () {
        return playerScore;
    }

    public void upTrue () {
        up = true;
        allowed = false;
    }

    public void upFalse () {
        up = false;
        allowed = true;
    }

    public void downTrue () {
        down = true;
    }

    public void downFalse () {
        down = false;
    }
}