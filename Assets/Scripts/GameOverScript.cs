﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverScript : MonoBehaviour {
    public Font theFont;
    public Texture bgImage;
    private int score = 0;
    private float buttStartY, buttLengthX, buttLengthY;
    private GUIStyle style, styleBlack, buttStyle, styleR, styleL, styleH;
    private bool high, pb, justPlayed, toUpload = false;
    private string name2, nameLower;
    private IDictionary<string, Int64> hash = new Dictionary<string, Int64> ();
    private List<KeyValuePair<string, Int64>> myList;

    void Start () {
        try {
            string temp = PlayerPrefs.GetString ("JustPlayed");
            if (temp.Equals ("true")) {
                justPlayed = true;
                PlayerPrefs.SetString ("JustPlayed", "false");
            } else {
                justPlayed = false;
            }
        } catch (Exception) { }
        name2 = PlayerPrefs.GetString ("name");
        nameLower = name2.ToLower ();
        score = PlayerPrefs.GetInt ("Score");
        buttStartY = (17f / 30f) * Screen.height;
        buttLengthX = (5f / 30f) * Screen.width;
        buttLengthY = (3f / 30f) * Screen.height;

        style = new GUIStyle ();
        style.alignment = TextAnchor.UpperCenter;
        style.fontSize = 36;
        style.font = theFont;
        style.normal.textColor = Color.white;

        styleL = new GUIStyle ();
        styleL.alignment = TextAnchor.UpperLeft;
        styleL.fontSize = 36;
        styleL.font = theFont;
        styleL.normal.textColor = Color.white;

        styleR = new GUIStyle ();
        styleR.alignment = TextAnchor.UpperRight;
        styleR.fontSize = 36;
        styleR.font = theFont;
        styleR.normal.textColor = Color.white;

        styleBlack = new GUIStyle ();
        styleBlack.alignment = TextAnchor.UpperCenter;
        styleBlack.fontSize = 36;
        styleBlack.font = theFont;
        styleBlack.normal.textColor = Color.black;

        styleH = new GUIStyle ();
        styleH.alignment = TextAnchor.MiddleCenter;
        styleH.fontSize = 50;
        styleH.font = theFont;
        styleH.normal.textColor = Color.black;

        buttStyle = new GUIStyle ("button");
        buttStyle.alignment = TextAnchor.MiddleCenter;
        buttStyle.fontSize = 25;
        buttStyle.font = theFont;

        StartCoroutine (GetText ());
        StartCoroutine (checkPB ());
    }

    IEnumerator GetText () {
        UnityWebRequest www = UnityWebRequest.Get ("https://zebra-runner-luke.firebaseio.com/board.json");
        yield return www.SendWebRequest ();
        if (www.isNetworkError || www.isHttpError) {
            Debug.Log (www.error);
        } else {
            hash = JsonConvert.DeserializeObject<Dictionary<string, Int64>> (www.downloadHandler.text);
            check ();
        }
    }

    void check () {
        myList = hash.ToList ();
        myList.Sort (
            delegate (KeyValuePair<string, Int64> pair1,
                KeyValuePair<string, Int64> pair2) {
                return pair2.Value.CompareTo (pair1.Value);
            }
        );
    }

    IEnumerator checkPB () {
        UnityWebRequest www = UnityWebRequest.Get ("https://zebra-runner-luke.firebaseio.com/board/" + nameLower + ".json");
        yield return www.SendWebRequest ();
        if (www.isNetworkError || www.isHttpError) {
            Debug.Log (www.error);
        } else {
            try {
                if (score > Int32.Parse (www.downloadHandler.text)) {
                    pb = true;
                    toUpload = true;
                    //StartCoroutine (Upload ());
                    hash[nameLower] = score;
                    Debug.Log ("addingUPDATE: " + hash[nameLower].ToString ());
                    // check ();
                }
            } catch (Exception) {
                pb = true;
                toUpload = true;
                hash.Add (nameLower, score);
                Debug.Log ("addingNew: " + hash[nameLower].ToString ());
                // check ();
            }
        }
        check ();
    }

    IEnumerator Upload () {
        byte[] myData = System.Text.Encoding.UTF8.GetBytes (score.ToString ());
        if (!(String.IsNullOrEmpty (nameLower))) {
            // string nameNew = nameCheck (nameLower));


            using (UnityWebRequest www = UnityWebRequest.Put ("https://zebra-runner-luke.firebaseio.com/board/" + nameLower + ".json", myData)) {
                yield return www.SendWebRequest ();
                // yield return SendWebRequest(www);
                if (www.isNetworkError || www.isHttpError) {
                    Debug.Log (www.error);
                } else {
                    Debug.Log ("Upload complete!");
                }
            }
        } else {
            Debug.Log ("ERROR - no name :(");
        }
    }

    void Update () { }

    void OnGUI () {
        GUI.Label (new Rect (0, (1f / 30f) * Screen.height, Screen.width, (2f / 30f) * Screen.height), "High scores", styleH);

        if (justPlayed) {
            if (pb) {
                GUI.Label (new Rect ((5f / 30f) * Screen.width, (4f / 30f) * Screen.height, (20f / 30f) * Screen.width, (18f / 30f) * Screen.height), "Score: " + score + ". New personal best!", styleBlack);
            } else {
                GUI.Label (new Rect ((5f / 30f) * Screen.width, (4f / 30f) * Screen.height, (20f / 30f) * Screen.width, (18f / 30f) * Screen.height), "Score: " + score, styleBlack);
            }
            if (high) {
                GUI.Label (new Rect ((5f / 30f) * Screen.width, (6f / 30f) * Screen.height, (20f / 30f) * Screen.width, (18f / 30f) * Screen.height), "NEW HIGH SCORE!", styleBlack);
            }
            if (GUI.Button (new Rect ((24f / 30f) * Screen.width, (22f / 30f) * Screen.height, buttLengthX, buttLengthY), "Change name", buttStyle)) {
                //Returns to menu
                SceneManager.LoadScene (4);
            }
        }

        if (GUI.Button (new Rect ((1f / 30f) * Screen.width, buttStartY, buttLengthX, buttLengthY), "Play Again", buttStyle) || Input.GetKeyUp (KeyCode.Space) || Input.GetKeyUp (KeyCode.Return)) {
            //Starts game
            SceneManager.LoadScene (5);
            if (toUpload) {
                StartCoroutine (Upload ());
            }
        }

        if (GUI.Button (new Rect ((24f / 30f) * Screen.width, buttStartY, buttLengthX, buttLengthY), "Return", buttStyle)) {
            //Returns to menu
            if (toUpload) {
                StartCoroutine (Upload ());
            }
            SceneManager.LoadScene (0);
        }
        GUI.Box (new Rect ((7f / 30f) * Screen.width, (9f / 30f) * Screen.height, (16f / 30f) * Screen.width, (14f / 30f) * Screen.height), bgImage);

        GUI.Label (new Rect ((5f / 30f) * Screen.width, (9f / 30f) * Screen.height, (20f / 30f) * Screen.width, (18f / 30f) * Screen.height), "TOP 5 ALL TIME:", style);

        GUI.Label (new Rect ((7.5f / 30f) * Screen.width, (12f / 30f) * Screen.height, (20f / 30f) * Screen.width, (18f / 30f) * Screen.height), "1. ", styleL);
        GUI.Label (new Rect ((7.5f / 30f) * Screen.width, (14f / 30f) * Screen.height, (20f / 30f) * Screen.width, (18f / 30f) * Screen.height), "2. ", styleL);
        GUI.Label (new Rect ((7.5f / 30f) * Screen.width, (16f / 30f) * Screen.height, (20f / 30f) * Screen.width, (18f / 30f) * Screen.height), "3. ", styleL);
        GUI.Label (new Rect ((7.5f / 30f) * Screen.width, (18f / 30f) * Screen.height, (20f / 30f) * Screen.width, (18f / 30f) * Screen.height), "4. ", styleL);
        GUI.Label (new Rect ((7.5f / 30f) * Screen.width, (20f / 30f) * Screen.height, (20f / 30f) * Screen.width, (18f / 30f) * Screen.height), "5. ", styleL);

        try {
            GUI.Label (new Rect ((8f / 30f) * Screen.width, (12f / 30f) * Screen.height, (14.5f / 30f) * Screen.width, (18f / 30f) * Screen.height), myList[0].Key.ToString () + " : " + myList[0].Value.ToString (), styleR);
            GUI.Label (new Rect ((8f / 30f) * Screen.width, (14f / 30f) * Screen.height, (14.5f / 30f) * Screen.width, (18f / 30f) * Screen.height), myList[1].Key.ToString () + " : " + myList[1].Value.ToString (), styleR);
            GUI.Label (new Rect ((8f / 30f) * Screen.width, (16f / 30f) * Screen.height, (14.5f / 30f) * Screen.width, (18f / 30f) * Screen.height), myList[2].Key.ToString () + " : " + myList[2].Value.ToString (), styleR);
            GUI.Label (new Rect ((8f / 30f) * Screen.width, (18f / 30f) * Screen.height, (14.5f / 30f) * Screen.width, (18f / 30f) * Screen.height), myList[3].Key.ToString () + " : " + myList[3].Value.ToString (), styleR);
            GUI.Label (new Rect ((8f / 30f) * Screen.width, (20f / 30f) * Screen.height, (14.5f / 30f) * Screen.width, (18f / 30f) * Screen.height), myList[4].Key.ToString () + " : " + myList[4].Value.ToString (), styleR);
        } catch (Exception) { }
    }
}