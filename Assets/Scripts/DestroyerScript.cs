﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DestroyerScript : MonoBehaviour {

    private HUDScript hud;

    void OnTriggerEnter2D (Collider2D other) {
        if (other.tag == "Player") {
            PlayerPrefs.SetString ("JustPlayed", "true");

            if (PlayerPrefs.HasKey ("name")) {
                hud = GameObject.Find ("Main Camera").GetComponent<HUDScript> ();
                PlayerPrefs.SetInt ("Score", hud.getPlayerScore ());
                //Goes to end screen
                SceneManager.LoadScene (2);
            } else {
                hud = GameObject.Find ("Main Camera").GetComponent<HUDScript> ();
                PlayerPrefs.SetInt ("Score", hud.getPlayerScore ());
                //Goes to Enter Name page (Facts)
                SceneManager.LoadScene (4);
            }
        }

        if (other.gameObject.transform.parent) {
            Destroy (other.gameObject.transform.parent.gameObject);
        } else {
            Destroy (other.gameObject);
        }
    }
}